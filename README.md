# nlp-metadata-meaning-detection

NLP project for Text mining Subject

07 October 2018

## Members

* MSE0084 - DO Tien Dung
* MSE0087 - DO Viet Quan
* MSE0088 - NGUYEN Ngoc Tram
* MSE0099 - NGUYEN Dinh Phuong

## Requirements

* Python 3.6.7
* Python pip

## How to use

### Project's configurations

Refer to file `const_value.py`, with the following values

* `devset_meta_data`     : File path to Devset Metadata file,      default `./data/devset_images_metadata.json`
* `devset_gt_data`       : File path to Devset GT file,            default `./data/devset_images_gt.csv`
* `testset_meta_data`    : File path to Testset Metadata file,     default `./data/testset_images_metadata.json`
* `testset_gt_data`      : File path to Testset GT data file,      default `./data/testset_images_gt.csv`
* `logical_negation`     : Array of logical negation words,        default `["not", "never", "no"]`
* `flood_words`          : Array of counting words,                default `['flood', 'floods', 'flooded', 'flooding']`
* `solution`             : Choose which solution to run (1|2|3|4), default `1`

### Installation

* Depending on your operating system, please install `python3-pip`
* `pip3 install wheel`
* `pip3 install pandas`
* `pip3 install nltk`
* `pip3 install sklearn`
* `python3 -m nltk.downloader punkt`

### Script files

* `train_naive_bayes.py` : Script to train NB model
* `test_naive_bayes.py`  : Script to test

### Commands

* `python3 train_naive_bayes.py`
* `python3 test_naive_bayes.py`
