class ImageMetaData():

    def __init__(self, img_id, description, title, user_tags):
        self.img_id = img_id
        self.description = description if description is not None else ""
        self.title = title if title is not None else ""
        self.user_tags = user_tags if user_tags is not None else []

    def __repr__(self):
        return str(self.__dict__)