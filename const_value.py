devset_meta_data = "./data/devset_images_metadata.json"
devset_gt_data = "./data/devset_images_gt.csv"
testset_meta_data = "./data/testset_images_metadata.json"
testset_gt_data = "./data/testset_images_gt.csv"

logical_negation = ["not", "never", "no"]
flood_words = ['flood', 'floods', 'flooded', 'flooding']

solution = 1