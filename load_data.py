#######################################################################################################################
# Import libraries
import json
import pandas as pd

from class_image import *


#######################################################################################################################
# Global variables and consts
devset_meta_data = "./data/devset_images_metadata.json"
devset_gt_data = "./data/devset_images_gt.csv"
path_to_output_csv = "./data/devset_images_bow.csv"


#######################################################################################################################
# Load meta data
def load_meta_data(file_path):
    with open(file_path) as f:
        json_object = json.load(f)
        images = json_object["images"]
    data = []
    for img in images:
        img_meta = ImageMetaData(img["image_id"], img["description"], img["title"], img["user_tags"])
        data.append(img_meta)
    return data


#######################################################################################################################
# Load label
def load_label(file_path):
    """Load gold-standard labels

    Return
    -------
    img2label: Map from image id to its label
    """
    df = pd.read_csv(file_path, header=None)
    img2label = {str(img_id): int(lb) for img_id, lb in zip(df[0], df[1])}
    return img2label


#######################################################################################################################
# Main
def main():
    devset_data = load_meta_data(devset_meta_data)
    print("Number of images: {}".format(len(devset_data)))

    # See some first images
    print(devset_data[:3])

    img2label = load_label(devset_gt_data)
    print(img2label["3595468464"], img2label["5090153632"])


#######################################################################################################################
if __name__ == '__main__':
    main()

