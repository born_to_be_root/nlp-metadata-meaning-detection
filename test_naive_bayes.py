#######################################################################################################################
# Import libraries
from load_data import *
from preprocess import *
from const_value import *


#######################################################################################################################
# Global variables and consts
global data_set
global no_of_items
global feature_set


#######################################################################################################################
# Write training data to txt file
def write_file(rows, file_name):
    file = open(file_name, mode='w', encoding='utf-8')

    for row in rows:
        s = '{} \n'.format(row)
        file.write(s)

    file.close()


#######################################################################################################################
# Load model
def load_model():
    global data_set
    global no_of_items
    global feature_set

    data_set = dict()
    no_of_items = dict()
    feature_set = dict()

    file = open('data_set.txt', mode='r', encoding='utf-8')
    file_data = file.read()
    lines = file_data.split('\n')[:-1]
    for line in lines:
        words = line.split(' ')
        key1 = int(words[0])
        key2 = words[1]
        value = int(words[2])

        data_set.setdefault(key1, dict())
        data_set[key1][key2] = value
    file.close()

    file = open('no_of_items.txt', mode='r', encoding='utf-8')
    file_data = file.read()
    lines = file_data.split('\n')[:-1]
    for line in lines:
        words = line.split(' ')
        key = int(words[0])
        value = int(words[1])
        no_of_items[key] = value
    file.close()

    file = open('feature_set.txt', mode='r', encoding='utf-8')
    file_data = file.read()
    lines = file_data.split('\n')[:-1]
    for line in lines:
        words = line.split(' ')
        key1 = words[0]
        key2 = int(words[1])
        value = int(words[2])
        feature_set.setdefault(key1, dict())
        feature_set[key1][key2] = value
    file.close()


#######################################################################################################################
# Calculate probability of word in category
def calc_prob(word, category):
    global feature_set
    global data_set

    if word not in feature_set or word not in data_set[category]:
        return 0

    return float(data_set[category][word]) / no_of_items[category]


#######################################################################################################################
# Weighted probability of a word for a category
def weighted_prob(word, category):
    # basic probability of a word - calculated by calc_prob
    basic_prob = calc_prob(word, category)

    # total_no_of_appearances - in all the categories
    if word in feature_set:
        tot = sum(feature_set[word].values())
    else:
        tot = 0

    # Weighted probability is given by the formula
    # (weight*assumedprobability + total_no_of_appearances*basic_probability)/(total_no_of_appearances+weight)
    # weight by default is taken as 1.0
    # assumed probability is 0.5 here
    weight_prob = ((1.0 * 0.5) + (tot * basic_prob)) / (1.0 + tot)
    return weight_prob


#######################################################################################################################
# To get probability of the test data for the given category
def test_prob(test, category):
    # Split the test data
    split_data = test.split(' ')

    data = []
    prefix = ''
    for i in split_data:
        if ' ' in i:
            i = i.split(' ')
            for j in i:
                word = prefix + j.lower()
                #word = j.lower()
                if word not in data:
                    data.append(word)

                if word.endswith('n\'t') or word in logical_negation:
                    if prefix == '':
                        prefix = 'NOT'
                    else:
                        prefix = ''
                else:
                    prefix = ''

                if len(j) <= 2:
                    prefix = ''

        elif len(i) > 2:
            word = prefix + i.lower()
            #word = i.lower()
            if word not in data:
                data.append(word)

            if word.endswith('n\'t') or word in logical_negation:
                if prefix == '':
                    prefix = 'NOT'
                else:
                    prefix = ''
            else:
                prefix = ''

        if len(i) <= 2:
            prefix = ''
    p = 1
    for i in data:
        p *= weighted_prob(i, category)
    return p


#######################################################################################################################
# Naive Bayes implementation
def naive_bayes(test):
    '''
		p(A|B) = p(B|A) * p(A) / p(B)

		Assume A - Category
			   B - Test data
			   p(A|B) - Category given the Test data

		Here ignoring p(B) in the denominator (Since it remains same for every category)
	'''
    results = dict()
    for i in data_set.keys():
        # Category Probability
        # Number of items in category/total number of items
        cat_prob = float(no_of_items[i]) / sum(no_of_items.values())

        # p(test data | category)
        test_prob1 = test_prob(test, i)

        results[i] = test_prob1 * cat_prob

    return results


#######################################################################################################################
# Return is_positive and is_neutral of different solution
def get_positive_and_neutral(has_flood_word, is_positive_naive_bayes):
    try:
        solution
    except NameError:
        solution_type = 1
    else:
        if solution > 4 or solution < 1 or not isinstance(solution, int):
            solution_type = 1
        else:
            solution_type = solution

    is_positive = False
    is_neutral = False

    # Solution 1: check if has flood words
    if solution_type == 1:
        if has_flood_word:
            is_positive = True

    # Solution 2: use Naive Bayes
    if solution_type == 2:
        if is_positive_naive_bayes:
            is_positive = True

    # Solution 3: combine Naive Bayes and has flood words
    if solution_type == 3:
        if is_positive_naive_bayes and has_flood_word:
            is_positive = True
        elif not is_positive_naive_bayes and not has_flood_word:
            is_positive = False
        else:
            is_neutral = True

    # Solution 4: if has flood words -> positive, else is positive naive bayes -> neutral
    if solution_type == 4:
        if has_flood_word:
            is_positive = True
        elif is_positive_naive_bayes:
            is_neutral = True

    return is_positive, is_neutral



#######################################################################################################################
# Predict result of test set
def test(testset_data):
    lst_positive = []
    lst_neutral = []
    lst_negative = []

    for img in testset_data:
        is_positive = False
        is_neutral = False

        has_flood_word = False
        is_positive_naive_bayes = False

        # Check if has flood words
        tokens = []
        text = clean_text(img.title)
        tokens += word_tokenize(text)

        for tag in img.user_tags:
            tag = clean_text(tag)
            if tag == "":
                continue
            for s in tag.split():
                tokens.append(s)
            '''tag = "_".join(tag.split())
            tokens.append(tag)'''

        for token in tokens:
            if token.lower() in flood_words:
                has_flood_word = True
                break

        # Use Naive Bayes
        text = extract_bow_features(img)
        r = naive_bayes(text)
        if r[1] > r[0]:
            is_positive_naive_bayes = True

        # Solution 1: check if has flood words
        # Solution 2: use Naive Bayes
        # Solution 3: combine Naive Bayes and has flood words -> Best result
        # Solution 4: if has flood words -> positive, else is positive naive bayes -> neutral
        is_positive, is_neutral = get_positive_and_neutral(has_flood_word, is_positive_naive_bayes)
            
        if is_positive:
            lst_positive.append(img.img_id)
        elif is_neutral:
            lst_neutral.append(img.img_id)
        else:
            lst_negative.append(img.img_id)

    return lst_positive + lst_neutral + lst_negative


#######################################################################################################################
# Main
def main():
    # Load training model
    print("Load training model")
    load_model()

    # Load meta data
    print("Load meta data")
    testset_data = load_meta_data(devset_meta_data)

    # Predict label of test set
    print("Predict result of test set")
    predicted_results = test(testset_data)

    # Get solution type
    solution_type = 1
    try:
        solution
    except NameError:
        solution_type = 1
    else:
        if solution > 4 or solution < 1 or not isinstance(solution, int):
            solution_type = 1
        else:
            solution_type = solution

    # Write predicted result to txt file
    print("Write predicted result to txt file using solution number {}".format(solution_type))
    write_file(predicted_results, 'testset_ranked_images.txt')


#######################################################################################################################
if __name__ == '__main__':
    main()

