#######################################################################################################################
# Import libraries
from load_data import *
from preprocess import *
from const_value import *


#######################################################################################################################
# Global variables and consts
global data_set
global no_of_items
global feature_set

devset_meta_data = "./data/devset_images_metadata.json"
devset_gt_data = "./data/devset_images_gt.csv"

logical_negation = ["not", "never", "no"]


#######################################################################################################################
# Write training data to txt file
def write_file():
    global data_set
    global no_of_items
    global feature_set

    file = open('data_set.txt', mode='w', encoding='utf-8')
    for key1 in data_set.keys():
        for key2 in data_set[key1].keys():
            s = '{} {} {}\n'.format(key1, key2, data_set[key1][key2])
            file.write(s)
    file.close()

    file = open('no_of_items.txt', mode='w', encoding='utf-8')
    for key in no_of_items.keys():
        s = '{} {}\n'.format(key, no_of_items[key])
        file.write(s)
    file.close()

    file = open('feature_set.txt', mode='w', encoding='utf-8')
    for key1 in feature_set.keys():
        for key2 in feature_set[key1].keys():
            s = '{} {} {}\n'.format(key1, key2, feature_set[key1][key2])
            file.write(s)
    file.close()


#######################################################################################################################
# Training
def train(devset_data, img2label):
    global data_set
    global no_of_items
    global feature_set

    # Dictionary that has the data :
    # { label(positive/negative) : { word : count of number of occurences of the word } }
    data_set = dict()

    # Dictionary that keeps the count of records that are labeled a label l for each label l
    # { label l : No. of records that are labeled l }
    no_of_items = dict()

    # Dictionary that contains the count of the occurences of word under each label
    # That is, { word : { label l : count of the occurence of word with label l } }
    feature_set = dict()

    for img in devset_data:
        text = extract_bow_features(img)
        label = img2label[img.img_id]

        # Initialize the label in the dictionary if not present already
        no_of_items.setdefault(label, 0)

        # Increase the count of occurence of label by 1 for every occurence
        no_of_items[label] += 1

        # Initialize the dictionary for a label if not present
        data_set.setdefault(label, dict())

        # Split the sentence with respect to non-characters, and donot split if apostophe is present
        split_data = text.split()

        # Deal with negation: no_of_itemprepend the prefix NOT to every word after a token of logical negation (not, never)
        # until the next punctuation mark.
        prefix = ''

        # For every word in split data
        for i in split_data:
            # Removing stop words to a small extent by ignoring words with length less than 2
            if len(i) > 1:
                word = prefix + i.lower()
                # word = i.lower()

                # Initialize the word count in data_set
                data_set[label].setdefault(word, 0)

                # Increase the word count on its occurence with label row[1]
                data_set[label][word] += 1

                # Initialze a dictionary for a newly found word in feature set
                feature_set.setdefault(word, dict())

                # If the label was found for the word, for the first time, initialize corresponding count value for word as key
                feature_set[word].setdefault(label, 0)

                # Increment the count for the word in that label
                feature_set[word][label] += 1

                if word.endswith('n\'t') or word in logical_negation:
                    if prefix == '':
                        prefix = 'NOT'
                    else:
                        prefix = ''
                else:
                    prefix = ''

            else:
                prefix = ''


#######################################################################################################################
# Main
def main():
    # Load meta data
    devset_data = load_meta_data(devset_meta_data)

    # Load label
    img2label = load_label(devset_gt_data)

    # Training
    train(devset_data, img2label)

    # Write training model to txt file
    write_file()


#######################################################################################################################
if __name__ == '__main__':
    main()

